﻿using System;
using System.Collections.Generic;
using System.Text;
using todoLists.Models;
using System.Linq;

namespace todoLists.Tests
{
    public class TodoListsDALStub : ITodoListsDAL
    {
        public List<TodoList> _todoLists;

        public TodoListsDALStub(List<TodoList> todoLists)
        {
            _todoLists = todoLists;
        }

        public void CreateTodoList(TodoList todoList)
        {
            if (_todoLists.Any(x => x.id == todoList.id))
                throw new ConflictException();
            _todoLists.Add(todoList);
        }

        public IEnumerable<TodoList> GetAllLists(int skip = 0, int limit = 0, string search = null)
        {
            var res = _todoLists;
            if (!string.IsNullOrEmpty(search))
            {
                res = res.Where(x => x.name.Contains(search)).ToList();
            }
            return res;
        }

        public TodoList GetList(Guid id)
        {
            if (!_todoLists.Any(list => list.id == id))
            {
                throw new KeyNotFoundException();
            }
            return _todoLists.First(list => list.id == id);
        }
    }
}
