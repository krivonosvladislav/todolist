using System;
using todoLists.Controllers;
using Xunit;
using Microsoft.AspNetCore.Mvc;
using todoLists.Models;
using System.Collections;
using System.Linq;

namespace todoLists.Tests
{
    public class TasksDALUnitTest
    {
        public DbContextStub db = new DbContextStub();
        public TodoController ctr = new TodoController(null);

        // lists Get

        [Fact]
        public void TodoControllerGetTestSuccess()
        {
            var res = ctr.Get("", 0, 0, db.todoListsDAL);
            Assert.IsType<OkObjectResult>(res);
        }

        [Fact]
        public void TodoControllerGetTestBadRequest()
        {
            var res = ctr.Get("", -1, 0, db.todoListsDAL);
            Assert.IsType<BadRequestResult>(res);
        }

        [Fact]
        public void TodoControllerGetTestEmptyResult()
        {
            IActionResult res = ctr.Get("asdf", 0, 0, db.todoListsDAL);
            Assert.IsType<OkObjectResult>(res);
            OkObjectResult okres = (OkObjectResult)res;
            Assert.Empty((IEnumerable)okres.Value);
        }

        // lists Post

        [Fact]
        public void TodoControllerPostTestSuccess()
        {
            Guid guid = Guid.NewGuid();
            TodoListsDALStub stub = (TodoListsDALStub)db.todoListsDAL;
            TodoList list = new TodoList()
            {
                id = guid,
                name = "fakeName",
                description = "fakeDescription",
                tasks = new System.Collections.Generic.List<Task>()
            };
            IActionResult res = ctr.Post(list, stub);
            Assert.IsType<StatusCodeResult>(res);
            StatusCodeResult scr = (StatusCodeResult) res;
            Assert.Equal(201, scr.StatusCode);
            Assert.Contains<TodoList>(list, stub._todoLists);
        }

        [Fact]
        public void TodoControllerPostTestBadRequest()
        {
            Guid guid = Guid.NewGuid();
            TodoList list = null;
            IActionResult res = ctr.Post(list, db.todoListsDAL);
            Assert.IsType<BadRequestResult>(res);
        }

        [Fact]
        public void TodoControllerPostTestConflict()
        {
            TodoListsDALStub stub = (TodoListsDALStub)db.todoListsDAL;

            Guid guid = stub._todoLists[0].id;
            TodoList list = new TodoList()
            {
                id = guid,
                name = "fakeName",
                description = "fakeDescription",
                tasks = new System.Collections.Generic.List<Task>()
            };
            IActionResult res = ctr.Post(list, stub);
            Assert.IsType<StatusCodeResult>(res);
            StatusCodeResult scr = (StatusCodeResult)res;
            Assert.Equal(409, scr.StatusCode);
        }

        // lists/{id} Get

        [Fact]
        public void TodoControllerGetListByIdTestSuccess()
        {
            // Pre setting for stub data
            TodoListsDALStub stub = (TodoListsDALStub)db.todoListsDAL;

            Guid guid = stub._todoLists[0].id;

            // Calling Action with stub data
            IActionResult res = ctr.Get(guid, stub);

            // Asserting that result is Success with needed value
            Assert.IsType<OkObjectResult>(res);
            OkObjectResult okRes = (OkObjectResult)res;
            Assert.IsType<TodoList>(okRes.Value);
            TodoList tdl = (TodoList)okRes.Value;
            Assert.Equal(guid, tdl.id);
        }

        [Fact]
        public void TodoControllerGetListByIdTestBadRequest()
        {
            // Pre setting for stub data
            TodoListsDALStub stub = (TodoListsDALStub)db.todoListsDAL;

            Guid guid = Guid.Empty;

            // Calling Action with stub data
            IActionResult res = ctr.Get(guid, stub);

            // Asserting that result is Bad Request
            Assert.IsType<BadRequestResult>(res);
        }

        [Fact]
        public void TodoControllerGetListByIdTestNotFound()
        {
            // Pre setting for stub data
            TodoListsDALStub stub = (TodoListsDALStub)db.todoListsDAL;

            Guid guid = Guid.NewGuid();

            // Calling Action with stub data
            IActionResult res = ctr.Get(guid, stub);

            // Asserting that result is Bad Request
            Assert.IsType<NotFoundResult>(res);
        }


        // list/{id}/tasks Post

        [Fact]
        public void TodoControllerPostCreateTaskTestSuccess()
        {
            TasksDALStub stub = (TasksDALStub)db.tasksDAL;
            Guid guid = Guid.NewGuid();
            Task task = new Task()
            {
                id = guid,
                name = "fakeNameTask",
                completed = false
            };

            Guid listGuid = stub._todoLists[0].id;

            IActionResult res = ctr.Post(listGuid, task, stub);
            Assert.IsType<StatusCodeResult>(res);
            StatusCodeResult scr = (StatusCodeResult)res;
            Assert.Equal(201, scr.StatusCode);
            Assert.Contains<Task>(task, stub._todoLists.First(x => x.id == listGuid).tasks);
        }

        [Fact]
        public void TodoControllerPostCreateTaskTestBadRequest()
        {
            TasksDALStub stub = (TasksDALStub)db.tasksDAL;

            Guid listGuid = stub._todoLists[0].id;
            Task task = null;
            IActionResult res = ctr.Post(listGuid, task, stub);
            Assert.IsType<BadRequestResult>(res);
        }

        [Fact]
        public void TodoControllerPostCreateTaskTestConflict()
        {
            TasksDALStub stub = (TasksDALStub)db.tasksDAL;
            Guid listGuid = stub._todoLists[0].id;
            Guid guid = stub._todoLists[0].tasks[0].id;
            Task task = new Task()
            {
                id = guid,
                name = "fakeNameTask2",
                completed = true
            };
            IActionResult res = ctr.Post(listGuid, task, stub);
            Assert.IsType<StatusCodeResult>(res);
            StatusCodeResult scr = (StatusCodeResult)res;
            Assert.Equal(409, scr.StatusCode);
        }

        // list/{id}/task/{taskId}/complete Post

        [Fact]
        public void TodoControllerPostUpdateTaskTestSuccess()
        {
            TasksDALStub stub = (TasksDALStub)db.tasksDAL;

            CompletedTask completedTask = new CompletedTask()
            {
                completed = false
            };

            Guid listGuid = stub._todoLists[0].id;
            Guid taskGuid = stub._todoLists[0].tasks[0].id;

            IActionResult res = ctr.Post(listGuid, taskGuid, completedTask, stub);

            Assert.IsType<StatusCodeResult>(res);
            StatusCodeResult scr = (StatusCodeResult)res;
            Assert.Equal(201, scr.StatusCode);
            Assert.Equal(completedTask.completed, stub._todoLists[0].tasks[0].completed);
        }

        [Fact]
        public void TodoControllerPostUpdateTaskTestBadRequest()
        {
            TasksDALStub stub = (TasksDALStub)db.tasksDAL;

            CompletedTask completedTask = null;

            Guid listGuid = stub._todoLists[0].id;
            Guid taskGuid = stub._todoLists[0].tasks[0].id;

            IActionResult res = ctr.Post(listGuid, taskGuid, completedTask, stub);
            Assert.IsType<BadRequestResult>(res);
        }
    }
}
