﻿using System;
using System.Collections.Generic;
using System.Text;
using todoLists.Models;

namespace todoLists.Tests
{
    public class DbContextStub
    {
        public ITodoListsDAL todoListsDAL;
        public ITasksDAL tasksDAL;

        public List<TodoList> todoLists;

        public DbContextStub()
        {
            GenerateTodoList();
            todoListsDAL = new TodoListsDALStub(todoLists);
            tasksDAL = new TasksDALStub(todoLists);
        }

        private void GenerateTodoList()
        {
            todoLists = new List<TodoList>();
            TodoList tdl = new TodoList()
            {
                id = Guid.NewGuid(),
                description = "stub1",
                name = "stub1",
                tasks = new List<Task>()
                {
                    new Task()
                    {
                        id = Guid.NewGuid(),
                        name = "stub11",
                        completed = true
                    },
                    new Task()
                    {
                        id = Guid.NewGuid(),
                        name = "stub12",
                        completed = false
                    }
                }
            };
            todoLists.Add(tdl);
        }
    }
}
