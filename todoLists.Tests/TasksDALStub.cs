﻿using System;
using System.Collections.Generic;
using System.Text;
using todoLists.Models;
using System.Linq;

namespace todoLists.Tests
{
    public class TasksDALStub : ITasksDAL
    {
        public List<TodoList> _todoLists;

        public TasksDALStub(List<TodoList> todoLists)
        {
            _todoLists = todoLists;
        }

        public void AddTaskToList(Guid listId, Task task)
        {
            if (_todoLists.Any(list => list.id == listId)
                    && _todoLists.First(list => list.id == listId).tasks.Any(t => t.id == task.id))
            {
                throw new ConflictException();
            }

            _todoLists.First(list => list.id == listId).tasks.Add(task);
        }

        public void UpdateTaskCompletedStatus(Guid listId, Guid taskId, CompletedTask updatedTask)
        {
            _todoLists.First(list => list.id == listId).tasks.First(task => task.id == taskId).completed = updatedTask.completed;
        }
    }
}
