﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace todoLists.Models
{
    public interface ITasksDAL
    {
        void AddTaskToList(Guid listId, Task task);

        void UpdateTaskCompletedStatus(Guid listId, Guid taskId, CompletedTask updatedTask);
    }
}
