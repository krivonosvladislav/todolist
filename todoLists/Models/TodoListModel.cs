﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace todoLists.Models
{
    public class TodoList
    {
        public Guid id { get; set; }
        [MaxLength(255)]
        public string name { get; set; }
        [MaxLength(255)]
        public string description { get; set; }
        public List<Task> tasks { get; set; }
    }
}
