﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace todoLists.Models
{
    public interface ITodoListsDAL
    {
        TodoList GetList(Guid id);

        IEnumerable<TodoList> GetAllLists(int skip = 0, int limit = 0, string search = null);

        void CreateTodoList(TodoList todoList);
    }
}
