﻿using System;
using System.Collections.Generic;
using System.Linq;
using todoLists.Models;
using Microsoft.EntityFrameworkCore;

namespace todoLists
{
    public class TodoListsDAL : ITodoListsDAL
    {
        public TodoList GetList(Guid id)
        {
            using (var db = new TodoListsDbContext())
            {
                if (!db.TodoLists.Any(list => list.id == id))
                    throw new KeyNotFoundException();
                return db.TodoLists.Include(l => l.tasks).First(list => list.id == id);    
            }
        }

        public IEnumerable<TodoList> GetAllLists(int skip = 0, int limit = 0, string search = null)
        {
            using (var db = new TodoListsDbContext())
            {
                IEnumerable<TodoList> res = db.TodoLists.Include(l => l.tasks);
                if (!string.IsNullOrEmpty(search))
                {
                    res = res.Where(list => list.name.Contains(search));
                }
                if (skip != 0)
                    res = res.Skip(skip);
                if (limit != 0)
                    res = res.Take(limit);
                return res.ToList();
            }
        }

        public void CreateTodoList(TodoList todoList)
        {
            using (var db = new TodoListsDbContext())
            {
                if (db.TodoLists.Any(list => list.id == todoList.id))
                    throw new ConflictException();
                db.TodoLists.Add(new TodoList() { id = todoList.id, name = todoList.name, description = todoList.description, tasks = todoList.tasks });
                db.SaveChanges();
            }
        }
    }
}
