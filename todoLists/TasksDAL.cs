﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using todoLists.Models;

namespace todoLists
{
    public class TasksDAL : ITasksDAL
    {
        public void AddTaskToList(Guid listId, Task task)
        {
            using (var db = new TodoListsDbContext())
            {
                if (db.TodoLists.Any(list => list.id == listId)
                    && db.TodoLists.Include(list => list.tasks).First(list => list.id == listId).tasks.Any(t => t.id == task.id))
                {
                    throw new ConflictException();
                }
                    
                db.TodoLists.Include(list => list.tasks).First(list => list.id == listId).tasks.Add(task);
                db.SaveChanges();
            }
        }

        public void UpdateTaskCompletedStatus(Guid listId, Guid taskId, CompletedTask updatedTask)
        {
            using (var db = new TodoListsDbContext())
            {
                db.TodoLists.Include(l => l.tasks).First(list => list.id == listId).tasks.First(task => task.id == taskId).completed = updatedTask.completed;
                db.SaveChanges();
            }
        }
    }
}
