﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using NLog;
using todoLists.Models;

namespace todoLists.Controllers
{
    public class TodoController : Controller
    {
        private readonly ILogger<TodoController> _logger;

        public TodoController(ILogger<TodoController> logger)
        {
            _logger = logger;
        }


        [HttpGet("lists")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public IActionResult Get(string searchString, int skip, int limit, [FromServices] ITodoListsDAL todoListsDAL)
        {
            _logger?.LogInformation("Get Request Started");
            if (!ModelState.IsValid || skip < 0 || limit < 0 )
            {
                _logger?.LogInformation("Model state Invalid | Returning 400 | Bad Request");
                return BadRequest();
            }
            _logger?.LogInformation("Model State Valid | Getting data from DB");
            var res = todoListsDAL.GetAllLists(skip, limit, searchString);
            _logger?.LogInformation("Data retrieval successful | Returning 200 | Success");
            return Ok(res);
        }

        [HttpPost("lists")]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(409)]
        public ActionResult Post([FromBody]TodoList value, [FromServices] ITodoListsDAL todoListsDAL)
        {
            _logger?.LogInformation("Post Request Started");
            if (!ModelState.IsValid || value == null)
            {
                _logger?.LogInformation("Model state Invalid | Returning 400 | Bad Request");
                return BadRequest();
            }
            try
            {
                _logger?.LogInformation("Model State Valid | Starting creation");
                todoListsDAL.CreateTodoList(value);
                _logger?.LogInformation("Creation successful | Returning 201 | Success");
                return new StatusCodeResult(201);
            }
            catch (ConflictException)
            {
                _logger?.LogInformation("Creation successful | Returning 409 | Conflict");
                return new StatusCodeResult(409);
            }
        }


        [HttpGet("list/{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public IActionResult Get(Guid id, [FromServices] ITodoListsDAL todoListsDAL)
        {
            _logger?.LogInformation("Get Request Started");
            if (!ModelState.IsValid || string.IsNullOrEmpty(id.ToString()) || id == Guid.Empty)
            {
                _logger?.LogInformation("Model state Invalid | Returning 400 | Bad Request");
                return BadRequest();
            }
            try
            {
                _logger?.LogInformation("Model State Valid | Getting data from DB");
                var res = todoListsDAL.GetList(id);
                _logger?.LogInformation("Data retrieval successful | Returning 200 | Success");
                return Ok(res);
            }
            catch (KeyNotFoundException)
            {
                _logger?.LogInformation("Data retrieval unsuccessful | Returning 404 | Key Not Found");
                return new NotFoundResult();
            }
        }

        [HttpPost("list/{id}/tasks")]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(409)]
        public ActionResult Post(Guid id, [FromBody]Task value, [FromServices] ITasksDAL tasksDAL)
        {
            _logger?.LogInformation("Post Request Started");
            if (!ModelState.IsValid || id == null || value == null)
            {
                _logger?.LogInformation("Model state Invalid | Returning 400 | Bad Request");
                return BadRequest();
            }
            try
            {
                _logger?.LogInformation("Model State Valid | Starting creation");
                tasksDAL.AddTaskToList(id, value);
                _logger?.LogInformation("Creation successful | Returning 201 | Success");
                return new StatusCodeResult(201);
            }
            catch (ConflictException)
            {
                _logger?.LogInformation("Creation successful | Returning 409 | Conflict");
                return new StatusCodeResult(409);
            }
        }

        [HttpPost("list/{id}/task/{taskId}/complete")]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public ActionResult Post(Guid id, Guid taskId, [FromBody]CompletedTask value, [FromServices] ITasksDAL tasksDAL)
        {
            _logger?.LogInformation("Post Request Started");
            if (!ModelState.IsValid || id == null || taskId == null || value == null)
            {
                _logger?.LogInformation("Model state Invalid | Returning 400 | Bad Request");
                return BadRequest();
            }
            _logger?.LogInformation("Model State Valid | Starting update");
            tasksDAL.UpdateTaskCompletedStatus(id, taskId, value);
            _logger?.LogInformation("Update successful | Returning 201 | Success");
            return new StatusCodeResult(201);
        }
    }
}
