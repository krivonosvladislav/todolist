﻿using Microsoft.EntityFrameworkCore;
using System;
using todoLists.Models;

namespace todoLists
{
    public class TodoListsDbContext : DbContext
    {
        public static string ConnectionString;

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlServer(ConnectionString);
        }

        public DbSet<TodoList> TodoLists { get; set; }
        public DbSet<Task> Tasks { get; set; }
    }
}