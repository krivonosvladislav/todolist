In Order for application to work you need to setup DB and run the project

To do that:

* Open solution in Visual Studio
* Open Package manager console (you can access it via Tools -> NuGet Package Manager -> Package Manager Console)
* Run the following command "update-database -verbose" ( This will cause migration scripts to run )
* Run the project
* Navigate to "/swagger"

*** SUGGESTIONS ***

Also i suggest to change the operation of modifying the Completed state of a task from POST to PATCH, because using POST leads to misundertanding and misinterpetation of HTTP methods.

HTTP methods are used like this:

* GET - retrieve information;
* POST - create an object;
* PUT - replace an object;
* PATCH - modify object state;
* DELETE - remove an object;

(as defined in RFC 7231, section 4 and RFC 5789 section 2)

From this we can see that using POST to modify object is irregular and should be used only in case of pursuing backward compatibility.